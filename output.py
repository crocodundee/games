import threading
import csv

FIELDS = ['game', 'draw_date', 'jackpot', 'balls']
FILE_NAME = 'games.csv'


def save_results(month, rows, filename=FILE_NAME, fields=FIELDS):
    lock = threading.Lock()
    with open(filename, "a") as file:
        file.write(f'Games in {month}:\n')
        writer = csv.DictWriter(file, fieldnames=fields)
        with lock:
            writer.writerows(rows)
