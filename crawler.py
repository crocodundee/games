import requests as req
import concurrent.futures as concurrent
from lxml import html
from threading import Thread
from multiprocessing import Process
from output import save_results


def get_games_results(dates):
    process = CrawlProcess(dates)
    process.start()
    process.join()


class CrawlThread(Thread):
    def __init__(self, url):
        self.response = html.fromstring(req.get(url).content)
        self.games = list()
        super(CrawlThread, self).__init__()

    def run(self):
        results = self.response.xpath('//a[@class="lottery-vertical-view-items clearfix"]')
        for r in results:
            game = r.xpath('div[@class="title-column"]/text()')[0]
            draw_date = r.xpath('div[@class = "date-column"]//text()')[0]
            jackpot = r.xpath('div[@class="jackpot-column"]/span/text()')[0]
            balls = r.xpath('div[@class="results-column"]/div/div[@class="lottery-ball-wrap"]/div/span/text()')
            result = {
                'game': game,
                'draw_date': draw_date,
                'jackpot': jackpot,
                'balls': balls
            }
            self.games.append(result)


class CrawlProcess(Process):
    def __init__(self, dates):
        self.month = dates.pop()
        self.urls = self.get_urls(dates)
        self.result = list()
        super(CrawlProcess, self).__init__()

    def get_urls(self, date_list):
        urls = [f'https://lottery.com/results/us/powerball/?start={date_list[i]}&end={date_list[i+1]}' for i in
                range(date_list.__len__() - 1)]
        return urls

    def get_week_games(self, url):
        thread = CrawlThread(url)
        thread.start()
        thread.join()
        self.result.extend(thread.games)

    def run(self):
        with concurrent.ThreadPoolExecutor() as executor:
            executor.map(self.get_week_games, self.urls)
        if self.result.__len__():
            save_results(self.month, self.result)

