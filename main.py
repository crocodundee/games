import concurrent.futures as concurrent
from datetime import datetime
from dates import get_dates_range
from crawler import get_games_results

if __name__ == '__main__':
    date_list = get_dates_range()
    start = datetime.now()
    with concurrent.ProcessPoolExecutor() as executor:
        executor.map(get_games_results, date_list)
    finish = datetime.now()
    print(f'Execute time: {finish - start}')



