import calendar
from datetime import datetime


def is_correct_date(start_date, month, date):
    return date.weekday() == 0 and date.month >= start_date and date.month == month and date <= datetime.today().date()


def get_month_date_list(start_date, stop_date, year):
    obj = calendar.Calendar(firstweekday=0)
    month_list = [calendar.month_name[i] for i in range(start_date, stop_date + 1)]
    date_list = [[date.strftime('%#m/%#d/%y') for date in obj.itermonthdates(year, i) if is_correct_date(start_date, i, date)]
                 for i in range(start_date, stop_date + 1)]
    return date_list, month_list


def get_crawl_date_list(date_list, month_list):
    crawl_date = list()
    for (date, month) in zip(date_list, month_list):
        date.append(month)
        crawl_date.append(date)
    return crawl_date


def get_dates_range():
    print('Enter month range: ')
    month_range = int(input())
    current_month = datetime.today().month
    current_year = datetime.today().year
    if month_range >= current_month:
        date_list, month_list = get_month_date_list(12 - (month_range-current_month), 12, current_year-1)
        date_list_tail, month_list_tail = get_month_date_list(1, current_month, current_year)
        date_list.extend(date_list_tail)
        month_list.extend(month_list_tail)
    else:
        date_list, month_list = get_month_date_list(current_month-month_range, current_month, current_year)
    for i in range(date_list.__len__()-1):
        date_list[i].append(date_list[i+1][0])
    crawl_date = get_crawl_date_list(date_list, month_list)
    return crawl_date
